<?php

namespace Serenata\Workspace\Configuration\Parsing;

use Exception;

/**
 * Indicates something went wrong during workspace configuration parsing.
 */
class WorkspaceConfigurationParsingException extends Exception
{
}
