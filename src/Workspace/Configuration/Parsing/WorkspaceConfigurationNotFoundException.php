<?php

namespace Serenata\Workspace\Configuration\Parsing;

/**
 * Indicates the workspace configuration was not found.
 */
class WorkspaceConfigurationNotFoundException extends WorkspaceConfigurationParsingException
{
}
